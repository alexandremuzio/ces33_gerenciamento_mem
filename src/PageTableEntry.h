#ifndef VIRTUAL_MEMORY_MANAGER_PAGETABLEENTRY_H
#define VIRTUAL_MEMORY_MANAGER_PAGETABLEENTRY_H

#include <chrono>

struct PageTableEntry {
    int virtualPage = 0;
    int physicalPage = 0;
    bool isPresent = 0;
    std::chrono::time_point<std::chrono::system_clock> lastUsedTime;

    bool operator<(const PageTableEntry &x) {
        return physicalPage < x.physicalPage;
    }

    bool operator==(const PageTableEntry &x) {
        return physicalPage == x.physicalPage;
    }
};

#endif //VIRTUAL_MEMORY_MANAGER_PAGETABLEENTRY_H
