#ifndef VIRTUAL_MEMORY_MANAGER_PAGETABLEFIFO_H
#define VIRTUAL_MEMORY_MANAGER_PAGETABLEFIFO_H

#include <cstdint>
#include <vector>
#include "PageTableEntry.h"
#include "SwapMemory.h"
#include "Queue.h"
#include "SegmentationTable.h"

class SegmentationTableFIFO: public SegmentationTable {
 private:
    std::vector<Queue<int>*> queueList;
    uint8_t *physicalMemory;
    std::vector<PageTableEntry*> segmentList;
    SwapMemory swapMemory;
    std::vector<int> currentPhysicalMemoryPage;
    int physicalMemorySize;
 public:
    SegmentationTableFIFO(uint8_t *physicalMemory, int physicalMemorySize);
    int getPhysicalPageFromVirtual(int segment, int virtualPage);
};

#endif //VIRTUAL_MEMORY_MANAGER_PAGETABLEFIFO_H
