#ifndef VIRTUAL_MEMORY_MANAGER_PAGETABLE_H
#define VIRTUAL_MEMORY_MANAGER_PAGETABLE_H

#define PAGE_SIZE 256
#define PAGE_TABLE_SIZE 256
#define PHYSICAL_SEGMENT_SIZE 128

class SegmentationTable {
 protected:
    int numberOfPageFaults;
    int numberOfSegmentationFaults;
 public:
    virtual int getPhysicalPageFromVirtual(int segment, int virtualPage) = 0;
    virtual int getNumberOfPageFaults() { return numberOfPageFaults; };
    virtual int getNumberOfSegmentationFaults() { return numberOfSegmentationFaults; };
};

#endif //VIRTUAL_MEMORY_MANAGER_PAGETABLE_H
