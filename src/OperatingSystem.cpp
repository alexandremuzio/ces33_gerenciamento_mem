#include "OperatingSystem.h"
#include <future>

OperatingSystem::OperatingSystem(int physicalMemorySize) :
    physicalMemorySize(physicalMemorySize) {
    physicalMemory = new uint8_t[physicalMemorySize * PAGE_TABLE_SIZE]{0};
    tlb = new TranslationLookasideBuffer(TLB_SIZE);
    pageTable = new SegmentationTableFIFO(physicalMemory, physicalMemorySize);
}

int OperatingSystem::getPhysicalAddressFromVirtual(int segment, int virtualAddress) {
    int physicalPage =
        tlb->getPhysicalPageFromVirtual(segment, getPageNumberFromAddress(virtualAddress));

    if (physicalPage == -1) {
        physicalPage =
            pageTable->getPhysicalPageFromVirtual(segment, getPageNumberFromAddress(virtualAddress));
        tlb->updateEntry(segment, getPageNumberFromAddress(virtualAddress), physicalPage);
    }

    return getPhysicalAddress(segment, physicalPage, virtualAddress);
}

uint8_t OperatingSystem::getContentFromPhysicalAddress(int segment, int physicalAddress) {
    return physicalMemory[32768 * segment + physicalAddress];
}

int OperatingSystem::getNumberOfPageFaults() {
    return pageTable->getNumberOfPageFaults();
}

int OperatingSystem::getNumberOfSegmentationFaults() {
    return pageTable->getNumberOfSegmentationFaults();
}

int OperatingSystem::getNumberOfTLBHits() {
    return tlb->getNumberOfHits();
}

int OperatingSystem::getPageNumberFromAddress(int address) {
    return (address >> 8);
}

int OperatingSystem::getPageOffsetFromAddress(int address) {
    return (uint8_t) address;
}

int OperatingSystem::getPhysicalAddress(int segment, int physicalPage, int virtualAddress) {
    return physicalPage * PAGE_SIZE
        + getPageOffsetFromAddress(virtualAddress);
}