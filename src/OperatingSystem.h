#ifndef VIRTUAL_MEMORY_MANAGER_OPERATINGSYSTEM_H
#define VIRTUAL_MEMORY_MANAGER_OPERATINGSYSTEM_H

#include "SwapMemory.h"
#include "PageTableEntry.h"
#include "SegmentationTableFIFO.h"
#include "TranslationLookasideBuffer.h"

#include <cstdint>
#include <cstring>

#define TLB_SIZE 16

class OperatingSystem {
 private:
    SegmentationTable *pageTable;
    TranslationLookasideBuffer *tlb;
    uint8_t *physicalMemory;
    int physicalMemorySize;
    int getPhysicalAddress(int segment, int physicalPage, int virtualAddress);
 public:
    OperatingSystem(int physicalMemorySize);
    int getPhysicalAddressFromVirtual(int segment, int virtualAddress);
    uint8_t getContentFromPhysicalAddress(int segment, int physicalAddress);
    int getNumberOfPageFaults();
    int getNumberOfSegmentationFaults();
    int getNumberOfTLBHits();
    int getPageNumberFromAddress(int address);
    int getPageOffsetFromAddress(int address);
};


#endif //VIRTUAL_MEMORY_MANAGER_OPERATINGSYSTEM_H
