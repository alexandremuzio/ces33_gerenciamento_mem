#ifndef VIRTUAL_MEMORY_MANAGER_TRANSLATIONLOOKASIDEBUFFER_H
#define VIRTUAL_MEMORY_MANAGER_TRANSLATIONLOOKASIDEBUFFER_H

#include <cstdint>
#include "TLBEntry.h"
#include "Queue.h"

class TranslationLookasideBuffer {
 private:
    Queue<TLBEntry> table;
    uint16_t numberOfHits;
 public:
    TranslationLookasideBuffer(uint16_t size);
    int getPhysicalPageFromVirtual(int segment, int logicalAddress);
    uint16_t getNumberOfHits();
    void updateEntry(int segment, int virtualPage, int physicalPage);
};


#endif //VIRTUAL_MEMORY_MANAGER_TRANSLATIONLOOKASIDEBUFFER_H
