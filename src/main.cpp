#include <iostream>
#include <fstream>
#include "OperatingSystem.h"

int main(int argc, char **argv) {
    if (argc != 2) {
        std::cout << "Invalid number of arguments" << std::endl;
        std::cout << "Expected: " << argv[0]
            << " ADDRESSES_TEXT_FILE" << std::endl;
        return 1;
    }

    std::ifstream inFileStream(argv[1], std::ios::in);
    if (!inFileStream.is_open()) {
        std::cout << "Error opening text file" << std::endl;
        return 1;
    }

    std::ofstream outFileStream("correct.txt", std::ios::out);
    if (!outFileStream.is_open()) {
        std::cout << "Error opening output text file" << std::endl;
        return 1;
    }

    int translationCount = 0;
    int address;
    int physicalAddress;
    int8_t memoryContent;
    OperatingSystem os(512);

    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();

    while (inFileStream.good()) {
        if (inFileStream >> address) {
            int currentSegment = translationCount % 4;
            outFileStream << "Virtual address: " << currentSegment
                << "-" << address;
            physicalAddress =
                os.getPhysicalAddressFromVirtual(currentSegment, address);
            outFileStream << " Physical address: " << currentSegment
                << "-" << physicalAddress;
            memoryContent = (int8_t) os.getContentFromPhysicalAddress(currentSegment, physicalAddress);
            outFileStream << " Value: " << std::to_string(memoryContent) << std::endl;
            translationCount++;
        }
    }

    end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end - start;
    std::cout << "Elapsed seconds: " << elapsed_seconds.count() << std::endl;

    outFileStream << "Number of Translated Addresses = " << translationCount << std::endl;
    outFileStream << "Segmentation Faults = " << os.getNumberOfSegmentationFaults() << std::endl;
    outFileStream << "Segmentation Fault Rate = " <<
        1.0 * os.getNumberOfSegmentationFaults() / translationCount << std::endl;
    outFileStream << "Page Faults = " << os.getNumberOfPageFaults() << std::endl;
    outFileStream << "Page Fault Rate = " <<
        1.0 * os.getNumberOfPageFaults() / translationCount << std::endl;
    outFileStream << "TLB Hits = " << os.getNumberOfTLBHits() << std::endl;
    outFileStream << "TLB Hit Rate = " <<
        1.0 * os.getNumberOfTLBHits() / translationCount << std::endl;

    return 0;
}