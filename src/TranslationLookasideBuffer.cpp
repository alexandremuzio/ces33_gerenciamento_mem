#include "TranslationLookasideBuffer.h"

TranslationLookasideBuffer::TranslationLookasideBuffer(uint16_t size)
    : table(size), numberOfHits(0) {
}

int TranslationLookasideBuffer::getPhysicalPageFromVirtual(int segment, int virtualPage) {
    TLBEntry toCompare;
    toCompare.virtualPage = virtualPage;
    toCompare.segment = segment;

    TLBEntry *entry = table.getEntry(toCompare);

    if (entry == nullptr) {
        return -1;
    }
    else {
        numberOfHits++;
        return entry->physicalPage;
    }
}

uint16_t TranslationLookasideBuffer::getNumberOfHits() {
    return numberOfHits;
}

void TranslationLookasideBuffer::updateEntry(int segment, int virtualPage, int physicalPage) {
    TLBEntry newEntry;
    newEntry.virtualPage = virtualPage;
    newEntry.physicalPage = physicalPage;
    newEntry.segment = segment;
    if (table.isFull()) {
        table.dequeue();
    }
    table.enqueue(newEntry);
}