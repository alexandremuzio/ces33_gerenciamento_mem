#ifndef VIRTUAL_MEMORY_MANAGER_TLBENTRY_H
#define VIRTUAL_MEMORY_MANAGER_TLBENTRY_H

#include <cstdint>

struct TLBEntry {
    int segment;
    int virtualPage;
    int physicalPage;

    bool operator<(const TLBEntry &other) {
        if (virtualPage == other.virtualPage) {
            return segment < other.segment;
        }
        return virtualPage < other.virtualPage;
    }

    bool operator==(const TLBEntry &other) {
        return virtualPage == other.virtualPage && segment == other.segment;
    }
};

#endif //VIRTUAL_MEMORY_MANAGER_TLBENTRY_H
