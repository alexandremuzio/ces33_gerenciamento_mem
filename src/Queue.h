#ifndef VIRTUAL_MEMORY_MANAGER_QUEUE_H
#define VIRTUAL_MEMORY_MANAGER_QUEUE_H

#include "TLBEntry.h"

template<typename T>
class Queue {
 private:
    T *queue;
    int size;
    int start;
    int end;

 public:
    Queue(int size) {
        queue = new T[size + 1];
        this->size = size;
        start = 0;
        end = 0;
    };

    bool isFull() {
        return (end + 1) % (size + 1) == start;
    }

    bool isEmpty() {
        return end == start;
    }

    void enqueue(T value) {
        queue[end] = value;
        end = (end + 1) % (size + 1);
    }

    T dequeue() {
        T temp = queue[start];
        start = (start + 1) % (size + 1);
        return temp;
    }

    T *getEntry(T virtualPage) {
        for (int i = 0; i <= size; i++) {
            if (queue[i] == virtualPage) {
                return &queue[i];
            }
        }
        return nullptr;
    }
};

#endif //VIRTUAL_MEMORY_MANAGER_QUEUE_H
