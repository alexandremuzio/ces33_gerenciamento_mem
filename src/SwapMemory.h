#ifndef VIRTUAL_MEMORY_MANAGER_SWAPMEMORY_H
#define VIRTUAL_MEMORY_MANAGER_SWAPMEMORY_H

#include <fstream>
#include <iostream>

#define BIN_FILE_NAME "../res/BACKING_STORE.bin"

class SwapMemory {
 private:
    std::ifstream *fileStream;
    long size;
    uint8_t *content;
 public:
    SwapMemory();
    uint8_t *readSwapAtPosition(int position);
};


#endif //VIRTUAL_MEMORY_MANAGER_SWAPMEMORY_H
