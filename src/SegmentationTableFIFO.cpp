#include "SegmentationTableFIFO.h"

SegmentationTableFIFO::SegmentationTableFIFO(uint8_t *physicalMemory, int physicalMemorySize) :
        physicalMemory(physicalMemory), physicalMemorySize(physicalMemorySize) {
    numberOfPageFaults = 0;
    numberOfSegmentationFaults = 0;
    for (int i = 0; i < 4; i++) {
        segmentList.push_back(nullptr);
        queueList.push_back(nullptr);
        currentPhysicalMemoryPage.push_back(-1);
    }
}

int SegmentationTableFIFO::getPhysicalPageFromVirtual(int segment, int virtualPage) {
    if (segmentList[segment] == nullptr) {
        numberOfSegmentationFaults++;
        segmentList[segment] = new PageTableEntry[PAGE_TABLE_SIZE];
        queueList[segment] = new Queue<int>(PHYSICAL_SEGMENT_SIZE);
    }

    if (!segmentList[segment][virtualPage].isPresent) {
        numberOfPageFaults++;
        uint8_t *page = swapMemory.readSwapAtPosition(virtualPage);
        segmentList[segment][virtualPage].isPresent = true;
        if (queueList[segment]->isFull()) {
            int removedIndex = queueList[segment]->dequeue();
            segmentList[segment][removedIndex].isPresent = false;
            currentPhysicalMemoryPage[segment] = segmentList[segment][removedIndex].physicalPage;
        }
        else {
            currentPhysicalMemoryPage[segment]++;
        }

        segmentList[segment][virtualPage].physicalPage = currentPhysicalMemoryPage[segment];
        queueList[segment]->enqueue(virtualPage);
        std::copy(page, page + PAGE_SIZE,
                  physicalMemory + PAGE_SIZE *
                  (segment * PHYSICAL_SEGMENT_SIZE + currentPhysicalMemoryPage[segment]));
    }
    return segmentList[segment][virtualPage].physicalPage;
}



