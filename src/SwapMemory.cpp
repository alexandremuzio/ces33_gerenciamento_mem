//
// Created by matheus on 5/22/16.
//

#include "SwapMemory.h"
#include "OperatingSystem.h"


SwapMemory::SwapMemory() {
    content = new uint8_t[PAGE_TABLE_SIZE];
    fileStream = new std::ifstream(BIN_FILE_NAME,
                                   std::ios::in | std::ios::binary | std::ios::ate);
    size = fileStream->tellg();
    fileStream->seekg(0, fileStream->beg);
}

uint8_t *SwapMemory::readSwapAtPosition(int position) {
    if (position >= size) return nullptr;
    fileStream->seekg(position * PAGE_TABLE_SIZE);
    fileStream->read((char *) content, PAGE_TABLE_SIZE);
    return content;
}





