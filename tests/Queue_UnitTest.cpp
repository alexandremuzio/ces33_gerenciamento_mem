#include "gtest/gtest.h"
#include "Queue.h"

TEST(Queue_UnitTest, isFull) {

    Queue<int> queue(5);

    EXPECT_FALSE(queue.isFull());
    queue.enqueue(1);
    EXPECT_FALSE(queue.isFull());
    queue.enqueue(2);
    EXPECT_FALSE(queue.isFull());
    queue.enqueue(3);
    EXPECT_FALSE(queue.isFull());
    queue.enqueue(4);
    EXPECT_FALSE(queue.isFull());
    queue.enqueue(5);
    EXPECT_TRUE(queue.isFull());
}

TEST(Queue_UnitTest, isEmpty) {

    Queue<int> queue(5);

    EXPECT_TRUE(queue.isEmpty());
    queue.enqueue(1);
    EXPECT_FALSE(queue.isEmpty());
    queue.dequeue();
    EXPECT_TRUE(queue.isEmpty());
    queue.enqueue(1);
    EXPECT_FALSE(queue.isEmpty());
    queue.enqueue(2);
    EXPECT_FALSE(queue.isEmpty());
    queue.dequeue();
    EXPECT_FALSE(queue.isEmpty());
    queue.dequeue();
    EXPECT_TRUE(queue.isEmpty());
    queue.enqueue(1);
    EXPECT_FALSE(queue.isEmpty());
    queue.enqueue(2);
    EXPECT_FALSE(queue.isEmpty());
    queue.enqueue(3);
    EXPECT_FALSE(queue.isEmpty());
    queue.enqueue(4);
    EXPECT_FALSE(queue.isEmpty());
    queue.enqueue(5);
    EXPECT_FALSE(queue.isEmpty());
    queue.dequeue();
    EXPECT_FALSE(queue.isEmpty());
    queue.dequeue();
    EXPECT_FALSE(queue.isEmpty());
    queue.dequeue();
    EXPECT_FALSE(queue.isEmpty());
    queue.dequeue();
    EXPECT_FALSE(queue.isEmpty());
    queue.dequeue();
    EXPECT_TRUE(queue.isEmpty());
}

TEST(Queue_UnitTest, dequeue) {

    Queue<int> queue(5);

    queue.enqueue(1);
    EXPECT_EQ(1, queue.dequeue());
    queue.enqueue(2);
    EXPECT_EQ(2, queue.dequeue());
    queue.enqueue(3);
    queue.enqueue(4);
    EXPECT_EQ(3, queue.dequeue());
    EXPECT_EQ(4, queue.dequeue());
    queue.enqueue(5);
    queue.enqueue(6);
    queue.enqueue(7);
    queue.enqueue(8);
    queue.enqueue(9);
    EXPECT_EQ(5, queue.dequeue());
    EXPECT_EQ(6, queue.dequeue());
    EXPECT_EQ(7, queue.dequeue());
    EXPECT_EQ(8, queue.dequeue());
    EXPECT_EQ(9, queue.dequeue());
}


int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}