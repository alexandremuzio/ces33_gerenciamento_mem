#include "gtest/gtest.h"
#include "SwapMemory.h"

TEST(SwapMemory_UnitTest, readSwapAtPosition) {

    SwapMemory memory;
    uint8_t *page;

    page = memory.readSwapAtPosition(0);
    for (int i = 0; i < 64; i++) {
        EXPECT_EQ(0, page[i * 4]);
        EXPECT_EQ(0, page[i * 4 + 1]);
        EXPECT_EQ(0, page[i * 4 + 2]);
        EXPECT_EQ(i, page[i * 4 + 3]);
    }

    page = memory.readSwapAtPosition(4);
    for (int i = 0; i < 64; i++) {
        EXPECT_EQ(0, page[i * 4]);
        EXPECT_EQ(0, page[i * 4 + 1]);
        EXPECT_EQ(1, page[i * 4 + 2]);
        EXPECT_EQ(i, page[i * 4 + 3]);
    }

    page = memory.readSwapAtPosition(5);
    for (int i = 0; i < 64; i++) {
        EXPECT_EQ(0, page[i * 4]);
        EXPECT_EQ(0, page[i * 4 + 1]);
        EXPECT_EQ(1, page[i * 4 + 2]);
        EXPECT_EQ(i + 64, page[i * 4 + 3]);
    }
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}